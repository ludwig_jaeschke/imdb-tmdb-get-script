Bekomme mit jQuery Ajax Requests eine IMDB/TMDB ID Liste von Film Titeln aus eine JSON Datei.

**MOVIES.JSON**

Die movies.json wurde unter Ubuntu mit hilfe vom 'ls'-Command erstellt.
-----------------

*# cd path/to/your/movies
# ls -1 | sed -e 's/\..*$//'*

-----------------
Und mit einen JSON-Generator zu einer validen JSON geformt.


**TMDB-API**

Für die Requests an die TMDB-API brauchst du als registrierter Developer ein API-Key:

http://docs.themoviedb.apiary.io/